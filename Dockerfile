FROM php:8.1.20-fpm-alpine3.18

WORKDIR /var/www/html

RUN apk --no-cache add git curl libpng libpng-dev libjpeg-turbo-dev libwebp-dev zlib-dev libxpm-dev freetype-dev libzip-dev

RUN docker-php-ext-install pdo pdo_mysql gd zip exif opcache

COPY --from=composer:latest /usr/bin/composer /usr/local/bin/composer

COPY . .

RUN chown -R www-data:www-data /var/www/html \
    && chmod -R 755 /var/www/html/storage

EXPOSE 9000
